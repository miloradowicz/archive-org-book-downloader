param(
  [Parameter(Position = 1)]
  [string]$url,

  [Parameter(Position = 2, ValueFromPipeline = $true)]
  [string]$cookie,

  [Parameter(Position = 3)]
  [string]$outputPath = 'book',

  [int]$startPage = 1,
  [int]$endPage,
  [string]$configJson = 'config.json',
  [string]$headersJson = 'headers.json'
)

$ProgressPreference = 'SilentlyContinue'

$contentType = 'multipart/form-data; boundary=---------------------------{0:d30}'
$formData = '-----------------------------{0:d30}
Content-Disposition: form-data; name="action"

{1}
-----------------------------{0:d30}
Content-Disposition: form-data; name="identifier"

{2}
-----------------------------{0:d30}--
'
enum Actions {
  join_waitlist
  leave_waitlist
  borrow_book
  browse_book
  availability
  create_token
  media_url
  return_loan
  search_doc_fields
  changes
}

$script:rnd = New-Object -TypeName System.Random

function GenerateRandomNumberString {
  param([int]$length)

  $boundary = [char]$rnd.Next([char]"`u{31}", [char]"`u{3a}")
  for ($j = 1; $j -lt $length; $j++) { $boundary += [char]$rnd.Next([char]"`u{30}", [char]"`u{3a}") }

  return $boundary
}

function HeadersToHashTable {
  param([string]$headers)

  return ConvertFrom-StringData -StringData ($headers -replace "; ", "`n")
}

function CookieJarToString {
  param([System.Collections.Hashtable]$cookieJar)

  return ($cookieJar.GetEnumerator() | ForEach-Object { "$($_.Name)=$($_.Value)" }) -join "; "
}

if ($PSBoundParameters.Count -eq 0) {
  Write-Host @'
USAGE  dl.ps1 [[-url] <Url>] [[-cookie] <Cookie>] [[-outputPath] <OutputPath>]
              [-startPage <StartPage>] [-endPage <EndPage>]
              [-configJson <ConfigJson>] [-headersJson <HeadersJson>]
'@
  return
}

if ($url -eq [System.String]::Empty) {
  throw 'No Url was provided.'
}

if ($cookie -eq [System.String]::Empty) {
  throw 'No Cookie was provided.'
}

$headersJson = [System.IO.Path]::Combine($PSScriptRoot, $headersJson)
$configJson = [System.IO.Path]::Combine($PSScriptRoot, $configJson)
$outputPath = [System.IO.Path]::Combine($PSScriptRoot, $outputPath)

if (-not (Test-Path -Path $headersJson -PathType Leaf)) {
  throw 'Could not find the headers file.'
}

if (-not (Test-Path -Path $configJson -PathType Leaf)) {
  throw 'Could not find the config file.'
}

if (-not (Test-Path $outputPath)) {
  [void](New-Item $outputPath -ItemType Directory)
}

$headers = Get-Content -Path $headersJson | ConvertFrom-Json -AsHashtable
$config = Get-Content -Path $configJson | ConvertFrom-Json

$pageRequest = Invoke-WebRequest -Uri $url -Headers $headers -SkipHttpErrorCheck

if ($pageRequest.StatusCode -ne [System.Net.HttpStatusCode]::OK) {
  throw 'Could not obtain the book page.'
}

$manifestRegex = '<ia-book-theater[^>]+(?<manifest>(?<=bookManifestUrl=")[^"]+)"[^>]*'

$match = [System.Text.RegularExpressions.Regex]::Match($pageRequest.Content, $manifestRegex)

if (-not $match.Groups['manifest'].Success) {
  throw 'Could not find the manifest Url on the book page.'
}

$manifestUrl = 'https:' + $match.Groups['manifest'].Value

Write-Host "Obtaining manifest....." -NoNewline
$manifestRequest = Invoke-WebRequest -Uri $manifestUrl -Headers $headers -SkipHttpErrorCheck

if ($manifestRequest.StatusCode -ne [System.Net.HttpStatusCode]::OK) {
  Write-Host "`e[38;5;229merror"

  throw 'Could not obtain the manifest.'
}
Write-Host "`e[38;5;40mdone"

$manifest = [System.Text.Json.Nodes.JsonNode]::Parse($manifestRequest.Content)
$brOptions = $manifest['data']['brOptions']

$referer = $url

$server = $brOptions['server']
$page = 'BookReader/BookReaderImages.php'
$zip = $brOptions['zip']
$imageFormat = $brOptions['imageFormat']
$bookId = $brOptions['bookId']
$subPrefix = $brOptions['subPrefix']
$pageCount = $brOptions['previewOrigNumLeafs']
$scale = $config.scale
$rotate = $config.rotate
$extension = $config.outputFileExtension
$filename = $config.outputFilenamePrefix

if (-not $PSBoundParameters.ContainsKey('endPage')) {
  $endPage = $pageCount
}

$cookieJar = HeadersToHashTable($cookie)
$headers['Referer'] = $referer
$headers['Cookie'] = CookieJarToString($cookieJar)

$loanRequest = Invoke-WebRequest -Uri 'https://archive.org/services/loans/loan' -Method Post -ContentType ($contentType -f $boundary) -Headers $headers -Body ($formData -f $boundary, [Actions]::browse_book, $bookId) -SkipHttpErrorCheck

if ($loanRequest.StatusCode -ne [System.Net.HttpStatusCode]::OK) {
  throw 'Could not borrow the book.'
}


$request = "https://${server}/${page}?zip=${zip}&file=${bookId}_${imageFormat}/${subPrefix}_{0:d4}.${imageFormat}&id=${bookId}&scale=${scale}&rotate=${rotate}"
$outputFile = "$outputPath/$filename{0:d4}.$extension"

try {
  $i = 0
  $fault = $false

  for ($i = $startPage; $i -le $endPage; $i++) {
    try {
      if (-not $fault) {
        Write-Host "Donwloading page `e[38;5;229m#$i`e[39m....." -NoNewline
      }

      Invoke-WebRequest -Uri ($request -f $i) -OutFile ($outputFile -f $i) -Headers $headers
      Write-Host "`e[38;5;40mdone"

      $fault = $false
    }
    catch {
      if ($fault) {
        Write-Host "`e[38;5;229merror"
        break;
      }

      $boundary = GenerateRandomNumberString(30)

      $tokenRequest = Invoke-WebRequest -Uri 'https://archive.org/services/loans/loan' -Method Post -ContentType ($contentType -f $boundary) -Headers $headers -Body ($formData -f $boundary, [Actions]::create_token, $bookId) -SkipHttpErrorCheck

      if ($tokenRequest.StatusCode -ne [System.Net.HttpStatusCode]::OK) {
        throw 'Could not renew the token.'
      }

      $tokenContent = ConvertFrom-Json -InputObject $tokenRequest.Content -AsHashtable
      $cookieJar["loan-$bookId"] = $tokenContent['token']

      $headers['Cookie'] = CookieJarToString($cookieJar)

      $fault = $true
      $i--
    }
  }
}
finally {
  Clear-Host

  if ($fault) {
    Write-Host "`e[38;5;213mCould not download page `e[38;5;229m#$i`e[38;5;213m. Your loan may have expired."
    Write-Host "Last page downloaded: `e[38;5;229m$( $i -eq $startPage ? 'none' : $i - 1) " -NoNewline
  }
  else {
    Write-Host "Download completed. " -NoNewline
  }

  Write-Host "Total pages downloaded: `e[38;5;229m$($i - $startPage)"
}

$ProgressPreference = 'Continue'